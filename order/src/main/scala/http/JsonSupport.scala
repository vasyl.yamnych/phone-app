package http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import models._
import spray.json.DefaultJsonProtocol

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {

  import JsonSupport._

  implicit val phoneFormat = jsonFormat2(Phone)
  implicit val jsonOrderFormat = jsonFormat4(JsonOrder)

  implicit val jsonCatalogFormat = jsonFormat1(Catalog)

  implicit val orderFormat = jsonFormat5(Order)
  implicit val orderResponseFormat = jsonFormat2(OrderResponse)
  implicit val jsonErrorFormat = jsonFormat2(JsonError)
  implicit val appErrorsResponseFormat = jsonFormat1(AppErrorsResponse)
}

object JsonSupport {

  case class Catalog(phones: List[Phone])

  case class JsonError(error_code: Int, phone_id: Option[Long])

  case class AppErrorsResponse(errors: List[JsonError])

  case class OrderResponse(order_id: Long, total_price: Double)
}