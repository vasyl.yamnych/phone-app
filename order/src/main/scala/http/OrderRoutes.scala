package http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import handlers.RequestHandler
import http.JsonSupport.{AppErrorsResponse, OrderResponse}
import http.utils.Mapping._
import models.JsonOrder

trait OrderRoutes extends JsonSupport {
  this: RequestHandler =>
  val route: Route =
    get {
      pathPrefix("orders") {
        pathSuffix(LongNumber) { orderId =>
          onSuccess(order(orderId)) {
            case Some(o) => complete(StatusCodes.OK, o)
            case _ => complete(StatusCodes.NotFound, s"order $orderId does not exists")
          }
        }
      }
    } ~ post {
      path("orders") {
        entity(as[JsonOrder]) { order =>
          onSuccess(newOrder(order)) {
            case Right(o) =>
              println(s"Total price = ${o.totalPrice}")
              complete(StatusCodes.Created, OrderResponse(o.id, o.totalPrice.doubleValue()))
            case Left(errors) => complete(StatusCodes.BadRequest, AppErrorsResponse(errors.map(_.toJsonError)))
          }
        }
      }
    }
}