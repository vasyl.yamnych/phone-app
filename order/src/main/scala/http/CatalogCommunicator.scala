package http

import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.http.scaladsl.model._
import akka.http.scaladsl.unmarshalling.Unmarshal
import akka.stream.ActorMaterializer
import http.JsonSupport.Catalog
import models.Phone
import spray.json.JsonParser

import scala.concurrent.Future

object CatalogCommunicator extends JsonSupport {
  implicit val system = ActorSystem("order-catalog-system")
  implicit val materializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  val (host, port) = ("catalog", 8087)

  def catalog: Future[List[Phone]] = Http().singleRequest(HttpRequest(method = HttpMethods.GET, uri = s"http://$host:$port/phones")).flatMap {
    case HttpResponse(StatusCodes.OK, _, entity, _) =>
      Unmarshal(entity).to[String].map {
        data => JsonParser(data).convertTo[Catalog].phones
      }
    case other =>
      val msg = s"Error communication with Catalog service: $other"
      throw new Exception(msg)
  }
}
