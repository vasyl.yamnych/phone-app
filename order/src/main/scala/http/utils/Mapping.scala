package http.utils

import http.JsonSupport.JsonError
import models.Errors.{AppError, PhoneError}

object Mapping {

  implicit class RichError(error: AppError) {
    def toJsonError = {
      val phoneId = error match {
        case m: PhoneError => Some(m.phoneId)
        case _ => None
      }
      JsonError(error.errorCode, phoneId)
    }
  }

}
