package handlers

import models.Errors.AppError
import models.{JsonOrder, Order}

import scala.concurrent.Future

trait RequestHandler {
  def order(id: Long): Future[Option[Order]]

  def newOrder(jsonOrder: JsonOrder): Future[Either[List[AppError], Order]]
}
