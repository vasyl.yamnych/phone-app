package handlers

import models.Errors.AppError
import models.utils.Mapping._
import models.{JsonOrder, Order}
import mongo.Connection.database
import mongo.Mapping._
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._
import services.CatalogValidator

import scala.concurrent.{ExecutionContext, Future}

class RequestHandlerMongoDB(catalogService: CatalogValidator) extends RequestHandler {
  implicit val ex = ExecutionContext.global

  val ordersCollection: MongoCollection[MongoOrder] = database.getCollection("orders")

  override def order(id: Long): Future[Option[Order]] = ordersCollection.find(equal("_id", id)).headOption().map(_.map(_.toOrder))

  override def newOrder(jsonOrder: JsonOrder): Future[Either[List[AppError], Order]] = {
    catalogService.checkAndCalcPrice(jsonOrder.items).flatMap {
      case Right(_) =>
        val mongoOrder = jsonOrder.toOrder(nextId).toMongoOrder
        ordersCollection.insertOne(mongoOrder).head().map(_ => Right(mongoOrder.toOrder))
      case Left(errors) => Future.successful(Left(errors))
    }
  }

  private def nextId = System.currentTimeMillis()
}