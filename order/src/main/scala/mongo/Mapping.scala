package mongo

import models.{Order, Phone}

object Mapping {

  case class MongoPhone(id: Long, price: Double) {
    def toPhone = Phone(id, BigDecimal(price))
  }

  case class MongoOrder(_id: Long, name: String, surname: String, email: String, items: List[MongoPhone]) {
    def toOrder = Order(_id, name, surname, email, items.map(_.toPhone))
  }

  implicit class RichOrder(order: Order) {
    def toMongoOrder = MongoOrder(order.id, order.name, order.surname, order.email, order.items.map(_.toMongoPhone))
  }

  implicit class RichPhone(phone: Phone) {
    def toMongoPhone = MongoPhone(phone.id, phone.price.doubleValue())
  }

}
