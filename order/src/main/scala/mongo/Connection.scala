package mongo

import mongo.Mapping._
import org.bson.codecs.configuration.CodecRegistries.{fromProviders, fromRegistries}
import org.mongodb.scala.bson.codecs.DEFAULT_CODEC_REGISTRY
import org.mongodb.scala.bson.codecs.Macros._
import org.mongodb.scala.{MongoClient, MongoDatabase}

object Connection {
  lazy val mongoClient = MongoClient(s"mongodb://$host:$port")
  lazy val database: MongoDatabase = mongoClient.getDatabase("ordersDB").withCodecRegistry(codecRegistry)
  private lazy val codecRegistry = fromRegistries(fromProviders(classOf[MongoOrder], classOf[MongoPhone]), DEFAULT_CODEC_REGISTRY)
  private lazy val (host, port) = ("mongo", 27017)
}
