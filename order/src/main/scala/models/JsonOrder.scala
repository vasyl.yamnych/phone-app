package models

case class JsonOrder(name: String, surname: String, email: String, items: List[Phone])
