package models

case class Order(id: Long, name: String, surname: String, email: String, items: List[Phone]) {
  def totalPrice: BigDecimal = items.foldLeft(BigDecimal(0))((b, phone) => b + phone.price)
}