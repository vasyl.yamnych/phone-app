package models

object Errors {

  sealed trait AppError {
    def errorCode: Int
  }

  abstract class PhoneError(val errorCode: Int) extends AppError {
    def phoneId: Long
  }

  case class PhoneIsAbsentAtCatalog(phoneId: Long) extends PhoneError(701)

  case class PhonePriceAtOrderDiffersFromCatalog(phoneId: Long, orderPrice: BigDecimal, catalogPrice: BigDecimal) extends PhoneError(702)

}
