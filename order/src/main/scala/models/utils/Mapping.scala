package models.utils

import models.{JsonOrder, Order}

object Mapping {

  implicit class RichJsonOrder(jsonOrder: JsonOrder) {
    def toOrder(orderId: Long) = Order(orderId, jsonOrder.name, jsonOrder.surname, jsonOrder.email, jsonOrder.items)
  }

}
