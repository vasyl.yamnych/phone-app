import akka.actor.ActorSystem
import akka.http.scaladsl.Http
import akka.stream.ActorMaterializer
import handlers.RequestHandlerMongoDB
import mongo.Connection
import http.OrderRoutes
import services.CatalogValidatorImpl

object Main extends App {
  implicit val system = ActorSystem("order-system")
  implicit val materializer = ActorMaterializer()
  // needed for the future flatMap/onComplete in the end
  implicit val executionContext = system.dispatcher

  val (host, port) = ("0.0.0.0", 8088)
  val catalogService = new CatalogValidatorImpl
  val orderRoutes = new RequestHandlerMongoDB(catalogService) with OrderRoutes
  val bindingFuture = Http().bindAndHandle(orderRoutes.route, host, port)

  bindingFuture.foreach(_ => println(s"Order service is online at http://$host:$port"))

  scala.sys.addShutdownHook {
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ => system.terminate().foreach(_ => materializer.shutdown())) // shutdown when done
    Connection.mongoClient.close()
  }
}
