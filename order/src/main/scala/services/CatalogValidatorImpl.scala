package services

import models.Errors._
import models.Phone
import http.CatalogCommunicator

import scala.concurrent.{ExecutionContext, Future}
import scala.util.Right

class CatalogValidatorImpl extends CatalogValidator {

  import CatalogValidatorImpl._

  override def checkAndCalcPrice(phones: List[Phone])(implicit ec: ExecutionContext): Future[Either[List[AppError], BigDecimal]] = {
    CatalogCommunicator.catalog.map { catalogPhones =>
      val pairs = zipPhones(phones, catalogPhones)
      val errors = findErrors(pairs)
      if (errors.isEmpty) Right(calcTotalPriceOfCorrectItems(pairs)) else Left(errors)
    }
  }
}

object CatalogValidatorImpl {
  def zipPhones(orderedPhones: List[Phone], catalog: List[Phone]): List[PhonePair] = {
    val catalogGroup = catalog.toVector
    orderedPhones.map(o => PhonePair(o, catalogGroup.find(_.id == o.id)))
  }

  def findErrors(pairs: List[PhonePair]): List[AppError] = pairs.foldLeft(List.empty[AppError])((b, pair) => pair match {
    case PhonePair(o, Some(c)) if o.price.equals(c.price) => b
    case PhonePair(o, Some(c)) => PhonePriceAtOrderDiffersFromCatalog(o.id, o.price, c.price) +: b
    case PhonePair(o, None) => PhoneIsAbsentAtCatalog(o.id) +: b
  })

  def calcTotalPriceOfCorrectItems(pairs: List[PhonePair]): BigDecimal = pairs.foldLeft(BigDecimal(0))((b, pair) => pair match {
    case PhonePair(o, Some(c)) if o.price.equals(c.price) => b + o.price
    case PhonePair(o, Some(c)) => b
    case PhonePair(o, None) => b
  })

  case class PhonePair(orderPhone: Phone, catalogPhone: Option[Phone])
}