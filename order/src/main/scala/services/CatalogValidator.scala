package services

import models.Errors.AppError
import models.Phone

import scala.concurrent.{ExecutionContext, Future}

trait CatalogValidator {
  def checkAndCalcPrice(phones: List[Phone])(implicit ec: ExecutionContext): Future[Either[List[AppError], BigDecimal]]
}
