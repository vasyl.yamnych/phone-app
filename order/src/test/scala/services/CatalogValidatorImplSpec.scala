package services

import models.Phone
import org.scalatest.{Matchers, WordSpec}
import services.CatalogValidatorImpl.PhonePair

class CatalogValidatorImplSpec extends WordSpec with Matchers {

  val phone1 = Phone(10, 100)
  val phone2 = Phone(20, 200)
  val phone3 = Phone(30, 300)
  val phone4 = Phone(40, 400)
  val phoneAbsent1 = Phone(1001, 901)
  val phoneAbsent2 = Phone(1002, 902)

  val phone2Wrong = Phone(20, 1020)
  val phone4Wrong = Phone(40, 1040)

  val catalog = List(phone1, phone2, phone3, phone4, phone4, phone4, phone3, phone2, phone1)

  val orderCorrect = List(phone4, phone3)
  val orderEmpty = List()
  val orderFewSamePhones = List(phone4, phone3, phone3, phone4, phone3, phone3)
  val orderAbsentAtCatalog = List(phone4, phoneAbsent1, phoneAbsent2, phone1)

  val orderBad = List(phone4, phone2Wrong, phone3)
  val order2Bad = List(phone4, phone2Wrong, phone4Wrong)

  "zipPhones func which groups order phones with catalog ones by phone id" should {
    "handle an order with diff phones as expected" in {
      CatalogValidatorImpl.zipPhones(orderCorrect, catalog) shouldEqual orderCorrect.map(o => PhonePair(o, Some(o)))
    }

    "handle an order with few same phones as expected" in {
      CatalogValidatorImpl.zipPhones(orderFewSamePhones, catalog) shouldEqual orderFewSamePhones.map(o => PhonePair(o, Some(o)))
    }

    "handle an empty order as expected" in {
      CatalogValidatorImpl.zipPhones(orderEmpty, catalog) shouldEqual List()
    }

    "handle an order with absent at catalog phones as expected" in {
      CatalogValidatorImpl.zipPhones(orderAbsentAtCatalog, catalog) shouldEqual orderAbsentAtCatalog.map(o => PhonePair(o, catalog.find(_.id == o.id)))
    }
  }

  //TODO TESTS for findErrors, calcTotalPriceOfCorrectItems
}
