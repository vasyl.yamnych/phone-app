package models

import org.scalatest.{Matchers, WordSpec}

class OrderSpec extends WordSpec with Matchers {

  val phone1 = Phone(10, 100)
  val phone2 = Phone(20, 200)
  val phone3 = Phone(30, 300)
  val items0 = List()
  val items1 = List(phone2)
  val items3 = List(phone1, phone2, phone3)

  val order1 = Order(1, "name", "surname", "email", items0)
  val order2 = Order(2, "name", "surname", "email", items1)
  val order3 = Order(3, "name", "surname", "email", items3)


  "Order" should {
    "calc total price = 0 for order with no phones" in {
      order1.totalPrice shouldEqual BigDecimal(0)
    }
    "calc total price with one phone at the order correctly" in {
      order2.totalPrice shouldEqual phone2.price
    }
    "calc total price when thera are a lot of phones at the order correctly" in {
      order3.totalPrice shouldEqual BigDecimal(600)
    }
  }

}
