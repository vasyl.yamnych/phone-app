package http

import akka.http.scaladsl.marshalling.Marshal
import akka.http.scaladsl.model.{MessageEntity, StatusCodes}
import akka.http.scaladsl.testkit.ScalatestRouteTest
import handlers.RequestHandler
import http.JsonSupport.OrderResponse
import models.Errors.AppError
import models.utils.Mapping._
import models.{JsonOrder, Order, Phone}
import org.scalatest.concurrent.ScalaFutures._
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.{ExecutionContext, Future}

class OrderRoutesSpec extends WordSpec with Matchers with ScalatestRouteTest with JsonSupport {

  val ordersPath = "/orders"

  val phone1 = Phone(50, 550)
  val phone2 = Phone(60, 650)
  val testItems = List(phone1, phone2)
  val orderId = 100
  val testOrder = Order(orderId, "name", "surname", "email", testItems)

  implicit val ec = ExecutionContext.global

  val ordersRoutes = new OrderRoutes with RequestHandler {
    private val orders = List(testOrder)

    override def order(id: Long): Future[Option[Order]] = Future.successful(orders.find(_.id == id))

    override def newOrder(jsonOrder: JsonOrder): Future[Either[List[AppError], Order]] = Future.successful(Right(jsonOrder.toOrder(orderId)))
  }

  "Order service" should {
    s"return a new order for POST requests to $ordersPath" in {
      val jsonOrder = JsonOrder("name1", "surname1", "email1", testItems)
      val orderEntity = Marshal(jsonOrder).to[MessageEntity].futureValue

      Post(s"$ordersPath").withEntity(orderEntity) ~> ordersRoutes.route ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.Created
        responseAs[OrderResponse] shouldEqual {
          val order = jsonOrder.toOrder(orderId)
          OrderResponse(order.id, order.totalPrice.doubleValue())
        }
      }
    }

    s"leave GET requests to $ordersPath unhandled" in {
      Get(s"$ordersPath") ~> ordersRoutes.route ~> check {
        handled shouldBe false
      }
    }

    s"return an order for GET requests to $ordersPath/{order_id}" in {
      Get(s"$ordersPath/${testOrder.id}") ~> ordersRoutes.route ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.OK
        responseAs[Option[Order]] shouldEqual Some(testOrder)
      }
    }

    s"return NOT FOUND status code for GET requests to $ordersPath/{order_id} if order id does not exists" in {
      Get(s"$ordersPath/100500") ~> ordersRoutes.route ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.NotFound
      }
    }
  }
}
