Phone App

Exercise 1: Create an endpoint to retrieve the phone catalog, and pricing.
* it returns a collection of phones, and their prices.
* Each phone should contain a reference to its image, the name, the description,
and its price.

Exercise 2: Create endpoints to check and create an order.
* receives and order that contains the customer information name, surname, and
email, and the list of phones that the customer wants to buy.
* Calculate the total prices of the order.
* Log the final order to the console.
Bonus Points: The second endpoint use the first endpoint to validate the order.

Requirements:
- It should have test.
- It should be documented in the readme file.
- It should be a REST API
- Docker oriented.
- Microservice approach.
- Database access from the microservices.
- Java 10, .NET, NodeJS, Scala

Questions
- How would you improve the system?
- How would you avoid your order API to be overflow?

==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ====

Order app validate phones' prices and their existence with Catalog app.

MongoDB is chosen as a data storage for Order and Catalog apps. 

Init-catalog app started only once and is needed for initial population of phone catalog to DB.

Quick start:

    Tools which must be installed:
    
        - Scala Build Tool (sbt) v1.1.6+
        - Docker 
        - Docker compose
    
    At a root dir of the phone-app project run the following commands at a command line:
        
        sbt ";order/assembly;catalog/assembly;initCatalog/assembly;docker"
    
        docker-compose up
    
    After that Catalog, Order, MongoDB, Init-catalog apps should be up and running.
    
    Init-catalog app will wait until MongoDB become available to populate it with initial phones data for Catalog app, will populate data and will terminate.

API:

    Catalog app:
  
       GET /phones - retrieve all phones
       GET /phones/{phone_id} - retrieve a phone by id

    Order app:
       
       GET /orders/{order_id} - retrieve an order by id
       POST /orders - create an order
  
Examples:

    Catalog app:
    
        * Get all phones
        curl -H "Content-Type: application/json" -X GET http://localhost:8087/phones
    
        * Get a phone by id
        curl -H "Content-Type: application/json" -X GET http://localhost:8087/phones/10
    
    Order app:
    
        * Create a new order
        curl -H "Content-Type: application/json" -X POST http://127.0.0.1:8088/orders \
          -d '{"name":"name1","surname":"surname1","email":"email1", "items": [{"id":10, "price": 100}, {"id":20, "price": 200}, {"id":30, "price": 300}]}' 
        
        JSON at Response:  
        {"order_id":1530889014218,"total_price":600.0}
        
        * Create order which should fail with 3 errors
        curl -H "Content-Type: application/json" -X POST http://127.0.0.1:8088/orders \
          -d '{"name":"name1","surname":"surname1","email":"email1", "items": [{"id":10, "price": 990}, {"id":11, "price": 110}, {"id":600, "price": 990}, {"id":500, "price": 990}]}'          
        
        JSON Error Response:  
        {"errors":[{"error_code":701,"phone_id":500},{"error_code":701,"phone_id":600},{"error_code":702,"phone_id":10}]}
        
        * Get an order by id
        curl -H "Content-Type: application/json" -X GET http://127.0.0.1:8088/orders/1530719992573
        
        JSON at Response:
        {"name":"name1","email":"email1","items":[{"id":10,"price":990}],"surname":"surname1","id":1530719992573}
       
==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ==== ====      
        
How would you improve the system?
  * Add service which helps to:
     * add a new phone to a catalog
     * update prices at catalog
  * For Catalog app add possibility to retrieve a set of phones instead of the full list of phones
  * Add application.conf files to each service and move env variables there
  * Tune logger, add logs
  * Write functional (integration) tests
  * Write performance, load tests

How would you avoid your order API to be overflow?
  * services are stateless, Mongo is horizontally scalable, thus, I could add a load balancer service on a top and scale catalog and order app horizontally.
  * send less data to order service: 
     * retrieve a set of phones from Catalog service instead of the full list of phones
     * send user id instead of the whole customer information        
   