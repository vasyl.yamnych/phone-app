name := "phone-app"

import Dependencies._

lazy val commonDependencies = Seq(
  Akka.core,
  Akka.stream,
  Akka.http,
  Akka.json,
  Akka.akkaSlf4j,
  Akka.httpTestKit,
  Logging.logback,
  ScalaTest.scalactic,
  ScalaTest.scalatest,
  Database.mongo
)

//can't find a plugin when this line is placed at plugins.sbt
addSbtPlugin("com.artima.supersafe" % "sbtplugin" % "1.1.7")

lazy val commonSettings = Seq(
  version := "0.1",
  organization := "vy",
  scalaVersion := "2.12.4"
  //test in assembly := {}
)

lazy val catalog = project.in(file("catalog"))
  .enablePlugins(DockerPlugin)
  .settings(
     assemblyJarName in assembly := "catalog.jar",
     commonSettings,
     dockerfileSettings("/catalog"),
     imageNameSettings("catalog"),
     imageNameSettings("catalog"),
     libraryDependencies ++= commonDependencies
  )

lazy val order = project.in(file("order"))
  .enablePlugins(DockerPlugin)
  .settings(
    assemblyJarName in assembly := "order.jar",
    commonSettings,
    dockerfileSettings("/order"),
    imageNameSettings("order"),
    libraryDependencies ++= commonDependencies
  )


lazy val initCatalog = project.in(file("init-catalog"))
  .enablePlugins(DockerPlugin)
  .settings(
    assemblyJarName in assembly := "init-catalog.jar",
    assemblyMergeStrategy in assembly := {
      case PathList("META-INF", xs @ _*) => MergeStrategy.discard
      case x => MergeStrategy.first
    },
    commonSettings,
    dockerfileSettings("/init-catalog"),
    imageNameSettings("init-catalog"),
    libraryDependencies ++= commonDependencies
  ).dependsOn(catalog)

def dockerfileSettings(path: String) = dockerfile in docker := {
  // The assembly task generates a fat JAR file
  val artifact: File = assembly.value
  val artifactTargetPath = s"$path/${artifact.name}"

  new Dockerfile {
    from("openjdk:8-jre")
    add(artifact, artifactTargetPath)
    entryPoint("java", "-jar", artifactTargetPath)
  }
}

def imageNameSettings(name: String) = imageNames in docker := Seq(
  // Sets a name with a tag that contains the project version
  ImageName(
    namespace = Some(organization.value),
    repository = name,
    tag = Some("v" + version.value)
  )
)