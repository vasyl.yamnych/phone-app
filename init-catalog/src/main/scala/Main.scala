import mongo.Connection
import mongo.Connection.database
import mongo.Mapping.MongoPhone
import org.mongodb.scala.MongoCollection
import org.mongodb.scala.model.Filters._

import scala.concurrent.duration._
import scala.concurrent.{Await, ExecutionContext, Future}

object Main extends App {
  implicit val executionContext = ExecutionContext.global

  Await.result(loop, Duration(10, MINUTES))
  Connection.mongoClient.close()

  lazy val count = 100
  lazy val phonesCollection: MongoCollection[MongoPhone] = database.getCollection("phones")

  def loop: Future[Any] = {
    populateData(count).flatMap(_ => Future.successful(println("Catalog data populated successfully"))).recoverWith {
      case ex: Exception =>
        println(s"Failed to populate data for Catalog. $ex")
        println("Another try...")
        loop
    }
  }

  def populateData(count: Int) = {
    phonesCollection.deleteMany(lte("_id", count)).toFuture().flatMap { _ =>
      val phones = (for {
        i <- 1 to count
      } yield MongoPhone(i, s"name_$i", s"imageRef_$i", s"description_$i", i * 10)).toList
      phonesCollection.insertMany(phones).toFuture()
    }
  }

}
