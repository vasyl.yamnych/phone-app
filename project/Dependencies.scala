import sbt._

object Dependencies {

  object Akka {
    val version = "2.5.12"
    val httpVersion = "10.1.3"

    val core = "com.typesafe.akka" %% "akka-actor" % version
    val stream = "com.typesafe.akka" %% "akka-stream" % version
    val http = "com.typesafe.akka" %% "akka-http" % httpVersion
    val json = "com.typesafe.akka" %% "akka-http-spray-json" % httpVersion
    val testKit = "com.typesafe.akka" %% "akka-testkit" % version % "test"
    val httpTestKit = "com.typesafe.akka" %% "akka-http-testkit" % httpVersion % "test"
    val akkaSlf4j = "com.typesafe.akka" %% "akka-slf4j" % version
  }

  object Logging {
    val logback = "ch.qos.logback" % "logback-classic" % "1.2.3"
  }

  object ScalaTest {
    val version = "3.0.5"
    val scalactic = "org.scalactic" %% "scalactic" % version
    val scalatest = "org.scalatest" %% "scalatest" % version % "test"
  }

  object Database {
    val mongo = "org.mongodb.scala" %% "mongo-scala-driver" % "2.4.0"
  }
}
