package http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.testkit.ScalatestRouteTest
import handlers.RequestHandler
import models.{Catalog, Phone}
import org.scalatest.{Matchers, WordSpec}

import scala.concurrent.Future

class PhoneRoutesSpec extends WordSpec with Matchers with ScalatestRouteTest with JsonSupport {

  val phonesPath = "/phones"

  val testPhone = Phone(100, "name", "imageRef", "description", 900)

  val phoneRoutes = new PhoneRoutes with RequestHandler {
    private val phones = List(testPhone)

    override def catalog: Future[Catalog] = Future.successful(Catalog(phones))

    override def phone(id: Long): Future[Option[Phone]] = Future.successful(phones.find(_.id == id))
  }

  "Phone catalog service" should {
    s"return a catalog of phones for GET requests to $phonesPath" in {
      Get(phonesPath) ~> phoneRoutes.route ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.OK
        responseAs[Catalog] shouldEqual Catalog(List(testPhone))
      }
    }

    s"return a phone for GET requests to $phonesPath/{phone_id}" in {
      Get(s"$phonesPath/${testPhone.id}") ~> phoneRoutes.route ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.OK
        responseAs[Phone] shouldEqual testPhone
      }
    }

    s"if phone is absent at catalog return the phone is not found response for GET requests to $phonesPath/{phone_id}" in {
      Get(s"$phonesPath/100500") ~> phoneRoutes.route ~> check {
        handled shouldBe true
        status shouldEqual StatusCodes.NotFound
      }
    }

    "leave GET requests to other paths unhandled" in {
      Get("/others") ~> phoneRoutes.route ~> check {
        handled shouldBe false
      }
    }
  }
}
