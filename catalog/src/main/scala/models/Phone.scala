package models

case class Phone(id: Long, name: String, imageRef: String, description: String, price: BigDecimal)
