package mongo

import models.Phone

object Mapping {

  case class MongoPhone(_id: Long, name: String, imageRef: String, description: String, price: Double) {
    def toPhone = Phone(_id, name, imageRef, description, BigDecimal(price))
  }

  implicit class RichPhone(phone: Phone) {
    def toMongoPhone = MongoPhone(phone.id, phone.name, phone.imageRef, phone.description, phone.price.doubleValue())
  }

}
