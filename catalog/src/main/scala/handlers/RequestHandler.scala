package handlers

import models.{Catalog, Phone}

import scala.concurrent.Future

trait RequestHandler {
  def catalog: Future[Catalog]

  def phone(id: Long): Future[Option[Phone]]
}
