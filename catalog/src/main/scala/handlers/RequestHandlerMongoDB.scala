package handlers

import models.{Catalog, Phone}
import mongo.Mapping.MongoPhone
import org.mongodb.scala._
import org.mongodb.scala.model.Filters._
import scala.concurrent.{ExecutionContext, Future}
import mongo.Connection._

class RequestHandlerMongoDB extends RequestHandler {
  implicit val ex = ExecutionContext.global

  val phonesCollection: MongoCollection[MongoPhone] = database.getCollection("phones")

  override def catalog: Future[Catalog] = phonesCollection.find().toFuture().map(ph => Catalog(ph.map(_.toPhone).toList))

  override def phone(id: Long): Future[Option[Phone]] = phonesCollection.find(equal("_id", id)).headOption().map(_.map(_.toPhone))

  private def nextId = System.currentTimeMillis()
}