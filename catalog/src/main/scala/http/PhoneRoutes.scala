package http

import akka.http.scaladsl.model.StatusCodes
import akka.http.scaladsl.server.Directives._
import akka.http.scaladsl.server.Route
import handlers.RequestHandler

trait PhoneRoutes extends JsonSupport {
  this: RequestHandler =>

  val route: Route =
    get {
      pathPrefix("phones") {
        pathEnd {
          onSuccess(catalog) { c =>
            complete(StatusCodes.OK, c)
          }
        } ~ pathSuffix(LongNumber) { phoneId =>
          onSuccess(phone(phoneId)) {
            case Some(ph) => complete(StatusCodes.OK, ph)
            case _ => complete(StatusCodes.NotFound, s"phone $phoneId does not exists")
          }
        }
      }
    }
}
