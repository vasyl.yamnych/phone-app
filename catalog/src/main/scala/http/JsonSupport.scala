package http

import akka.http.scaladsl.marshallers.sprayjson.SprayJsonSupport
import spray.json._
import models.{Catalog, Phone}

trait JsonSupport extends SprayJsonSupport with DefaultJsonProtocol {
  implicit val phoneFormat = jsonFormat5(Phone)
  implicit val catalogFormat = jsonFormat1(Catalog)
}
